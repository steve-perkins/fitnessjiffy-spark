package com.steveperkins.fitnessjiffy.dto.converter;

import com.steveperkins.fitnessjiffy.domain.FoodEaten;
import com.steveperkins.fitnessjiffy.dto.FoodEatenDTO;

import javax.annotation.Nullable;

public final class FoodEatenToFoodEatenDTO {

    private final FoodToFoodDTO foodDTOConverter = new FoodToFoodDTO();

    @Nullable
    public FoodEatenDTO convert(@Nullable final FoodEaten foodEaten) {
        FoodEatenDTO dto = null;
        if (foodEaten != null) {
            dto = new FoodEatenDTO(
                    foodEaten.getId(),
                    foodEaten.getUser().getId(),
                    foodDTOConverter.convert(foodEaten.getFood()),
                    foodEaten.getDate(),
                    foodEaten.getServingType(),
                    foodEaten.getServingQty(),
                    foodEaten.getCalories(),
                    foodEaten.getFat(),
                    foodEaten.getSaturatedFat(),
                    foodEaten.getSodium(),
                    foodEaten.getCarbs(),
                    foodEaten.getFiber(),
                    foodEaten.getSugar(),
                    foodEaten.getProtein(),
                    foodEaten.getPoints()
            );
        }
        return dto;
    }

}
