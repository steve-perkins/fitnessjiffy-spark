package com.steveperkins.fitnessjiffy.dto.converter;

import com.steveperkins.fitnessjiffy.domain.ExercisePerformed;
import com.steveperkins.fitnessjiffy.dto.ExercisePerformedDTO;

import javax.annotation.Nullable;

public class ExercisePerformedToExercisePerformedDTO {

    private final ExerciseToExerciseDTO exerciseDTOConverter = new ExerciseToExerciseDTO();

    @Nullable
    public ExercisePerformedDTO convert(@Nullable final ExercisePerformed exercisePerformed) {
        ExercisePerformedDTO dto = null;
        if (exercisePerformed != null) {
            dto = new ExercisePerformedDTO();
            dto.setId(exercisePerformed.getId());
            dto.setUserId(exercisePerformed.getUser().getId());
            dto.setExercise(exerciseDTOConverter.convert(exercisePerformed.getExercise()));
            dto.setDate(exercisePerformed.getDate());
            dto.setMinutes(exercisePerformed.getMinutes());
        }
        return dto;
    }

}
