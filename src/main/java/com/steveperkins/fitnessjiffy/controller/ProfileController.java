package com.steveperkins.fitnessjiffy.controller;

import com.steveperkins.fitnessjiffy.domain.User;
import com.steveperkins.fitnessjiffy.dto.UserDTO;
import com.steveperkins.fitnessjiffy.dto.WeightDTO;
import com.steveperkins.fitnessjiffy.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.TemplateViewRoute;

import javax.annotation.Nonnull;
import java.sql.Date;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.UUID;

public class ProfileController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Nonnull
    public final TemplateViewRoute viewMainProfilePage() {
        return (request, response) -> {
            final Map<String, Object> model = new HashMap<>();
            try {
                final String dateString = request.queryParams("date");

                final UserDTO userDTO = request.session().attribute("user");
                final Date date = dateString == null ? todaySqlDateForUser(userDTO) : stringToSqlDate(dateString);
                final WeightDTO weight = new UserService().findWeightOnDate(userDTO, date);
                final String weightEntry = (weight == null) ? "" : String.valueOf(weight.getPounds());
                final int heightFeet = (int) (userDTO.getHeightInInches() / 12);
                final int heightInches = (int) userDTO.getHeightInInches() % 12;

                model.put("allActivityLevels", User.ActivityLevel.values());
                model.put("allGenders", User.Gender.values());
                model.put("allTimeZones", new TreeSet<>(ZoneId.getAvailableZoneIds()));
                model.put("user", userDTO);
                model.put("dateString", dateString);
                model.put("weightEntry", weightEntry);
                model.put("heightFeet", heightFeet);
                model.put("heightInches", heightInches);
            } catch (Exception e) {
                logger.error("ProfileController.viewMainProfilePage", e);
            }
            return new ModelAndView(model, PROFILE_TEMPLATE);
        };
    }

    @Nonnull
    public final TemplateViewRoute updateProfile() {
        return (request, response) -> {
            final Map<String, Object> model = new HashMap<>();
            try {
                final UUID id = UUID.fromString(request.queryParams("id"));
                final String email = request.queryParams("email");
                final String currentPassword = request.queryParams("currentPassword");
                final String newPassword = request.queryParams("newPassword");
                final String reenterNewPassword = request.queryParams("reenterNewPassword");
                final String firstName = request.queryParams("firstName");
                final String lastName = request.queryParams("lastName");
                final User.Gender gender = User.Gender.fromString(request.queryParams("gender"));
                final java.sql.Date birthdate = new java.sql.Date(dateFormat.parse(request.queryParams("birthdate")).getTime());
                int heightFeet = 0;
                try {
                    heightFeet = Integer.parseInt(request.queryParams("heightFeet"));
                } catch (Exception e) {
                }
                int heightInches = 0;
                try {
                    heightInches = Integer.parseInt(request.queryParams("heightInches"));
                } catch (Exception e) {
                }
                final User.ActivityLevel activityLevel = User.ActivityLevel.fromString(request.queryParams("activityLevel"));
                final String timeZone = request.queryParams("timeZone");
                final UserDTO userDTO = new UserDTO(
                        id,
                        gender,
                        birthdate,
                        heightFeet * 12 + heightInches,
                        activityLevel,
                        email,
                        firstName,
                        lastName,
                        timeZone,
                        0,
                        0,
                        0,
                        0
                );

                final UserService userService = new UserService();
                if (currentPassword == null || currentPassword.isEmpty()) {
                    model.put("profileSaveError", "You must verify the current password in order to make any changes to this profile.");
                } else if (!userService.verifyPassword(userDTO, currentPassword)) {
                    model.put("profileSaveError", "The password entered does not match the current password.");
                } else if (newPassword != null && !newPassword.isEmpty() && reenterNewPassword != null && !reenterNewPassword.equals(newPassword)) {
                    model.put("profileSaveError", "The 'New Password' and 'Re-enter New Password' fields did not match.");
                } else {
                    // Update user in the database
                    if (newPassword.isEmpty()) {
                        userService.updateUser(userDTO);
                    } else {
                        userService.updateUser(userDTO, newPassword);
                    }

                    // Update the user in the active Spring Security session
                    final UserDTO updatedUser = userService.findUser(userDTO.getId());  // re-calc BMI, daily calorie needs, etc
                    request.session().attribute("user", updatedUser);
                }
                response.redirect("/profile");
            } catch (Exception e) {
                logger.error("ProfileController.updateProfile", e);
            }
            return null;
        };
    }

    @Nonnull
    public final TemplateViewRoute createOrUpdateWeight(
    ) {
        return (request, response) -> {
            final String dateString = request.queryParams("date");
            double weightEntry = 0;
            try {
                weightEntry = Double.parseDouble(request.queryParams("weightEntry"));
            } catch (Exception e) {
            }
            final UserDTO userDTO = request.session().attribute("user");
            final Date date = dateString == null ? todaySqlDateForUser(userDTO) : stringToSqlDate(dateString);
            new UserService().updateWeight(userDTO, date, weightEntry);

            // Update the user in the active Spring Security session
            final UserDTO updatedUser = new UserService().findUser(userDTO.getId());  // re-calc BMI, daily calorie needs, etc
            request.session().attribute("user", updatedUser);

            response.redirect("/profile?dateString=" + dateString);
            return null;
        };
    }

}
