package com.steveperkins.fitnessjiffy.controller;

import com.steveperkins.fitnessjiffy.dto.UserDTO;
import com.steveperkins.fitnessjiffy.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.TemplateViewRoute;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class LoginController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Nonnull
    public final TemplateViewRoute viewLoginPage() {
        return (request, response) -> new ModelAndView(new HashMap<>(), LOGIN_TEMPLATE);
    }

    @Nonnull
    public final TemplateViewRoute viewLoginError() {
        return ((request, response) -> {
            final Map<String, Object> model = new HashMap<>();
            model.put("error", true);
            return new ModelAndView(model, LOGIN_TEMPLATE);
        });
    }

    @Nonnull
    public final TemplateViewRoute doLogin() {
        return (((request, response) -> {
            final String username = request.queryParams("username");
            final String password = request.queryParams("password");
            final UserService userService = new UserService();

            final UserDTO userDTO = userService.authenticate(username, password);
            if (userDTO == null) {
                response.redirect("/login/error");
            } else {
                request.session(true).attribute("user", userDTO);
                response.redirect("/profile");
            }
            return null;
        }));
    }

    @Nonnull
    public final TemplateViewRoute doLogout() {
        return ((request, response) -> {
            request.session().invalidate();
            final Map<String, Object> model = new HashMap<>();
            model.put("logout", true);
            return new ModelAndView(model, LOGIN_TEMPLATE);
        });
    }
}
