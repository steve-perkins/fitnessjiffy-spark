package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.Food;
import com.steveperkins.fitnessjiffy.domain.User;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

public class FoodRepository implements Repository<Food, UUID> {

    private final EntityManager entityManager;

    public FoodRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Food findOne(UUID key) {
        return entityManager.find(Food.class, key);
    }

    @Override
    public List<Food> findAll() {
        return entityManager.createQuery("SELECT food FROM Food food").getResultList();
    }

    @Override
    public void save(Food entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(Food entity) {
        entityManager.remove(entity);
    }

    /**
     * Returns all "global" foods (i.e. where ownerId is null).
     */
    @Nonnull
    public List<Food> findByOwnerIsNull() {
        return entityManager.createQuery("SELECT food FROM Food food WHERE food.owner = null").getResultList();
    }

    /**
     * Returns all foods owned by a particular user.
     */
    @Nonnull
    public List<Food> findByOwner(@Nonnull final User owner) {
        return entityManager.createQuery("SELECT food FROM Food food WHERE food.owner = :owner")
                .setParameter("owner", owner)
                .getResultList();
    }

    /**
     * Returns all foods visible to a particular user.  This includes the foods that they own, as well as all global
     * foods.  When a user customizes a global food, an owned copy of that food is made for the user, rather than
     * actually modifying the global food itself.  Therefore, any global foods matching the name of an owned food
     * is filtered out of these results.
     */
    @Nonnull
    public List<Food> findVisibleByOwner(@Nonnull final User owner) {
        return entityManager.createQuery(
                "SELECT food FROM Food food WHERE food.owner = :owner "
                        + "OR ("
                        + "food.owner IS NULL "
                        + "AND NOT EXISTS (SELECT subFood FROM Food subFood WHERE subFood.owner = :owner AND subFood.name = food.name)"
                        + ") ORDER BY food.name ASC")
                .setParameter("owner", owner)
                .getResultList();
    }

    @Nonnull
    public List<Food> findByNameLike(
            @Nonnull final User owner,
            @Nonnull final String name
    ) {
        return entityManager.createQuery(
                "SELECT food FROM Food food "
                        + "WHERE ("
                        + "   food.owner = :owner "
                        + "   OR ("
                        + "      food.owner IS NULL "
                        + "      AND NOT EXISTS (SELECT subFood FROM Food subFood WHERE subFood.owner = :owner AND subFood.name = food.name)"
                        + "   )"
                        + ") AND LOWER(food.name) LIKE LOWER(CONCAT('%', :name, '%')) "
                        + "ORDER BY food.name ASC")
                .setParameter("owner", owner)
                .setParameter("name", name)
                .getResultList();
    }

    @Nonnull
    public List<Food> findByOwnerEqualsAndNameEquals(
            @Nonnull final User owner,
            @Nonnull final String name
    ) {
        return entityManager.createQuery(
                "SELECT food FROM Food food WHERE food.owner = :owner AND food.name = :name")
                .setParameter("owner", owner)
                .setParameter("name", name)
                .getResultList();
    }

}
