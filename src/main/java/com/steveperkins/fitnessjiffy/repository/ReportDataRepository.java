package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.ReportData;
import com.steveperkins.fitnessjiffy.domain.User;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class ReportDataRepository implements Repository<ReportData, UUID> {

    private final EntityManager entityManager;

    public ReportDataRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public ReportData findOne(UUID key) {
        return entityManager.find(ReportData.class, key);
    }

    @Override
    public List<ReportData> findAll() {
        return entityManager.createQuery("SELECT reportData from ReportData reportData").getResultList();
    }

    @Override
    public void save(ReportData entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(ReportData entity) {
        entityManager.remove(entity);
    }

    @Nonnull
    public List<ReportData> findByUserOrderByDateAsc(@Nonnull final User user) {
        return entityManager.createQuery(
                "SELECT reportData FROM ReportData reportData WHERE reportData.user = :user ORDER BY reportData.date ASC")
                .setParameter("user", user)
                .getResultList();
    }

    @Nonnull
    public List<ReportData> findByUserAndDateOrderByDateAsc(
            @Nonnull final User user,
            @Nonnull final Date date
    ) {
        return entityManager.createQuery(
                "SELECT reportData FROM ReportData reportData "
                    + "WHERE reportData.user = :user "
                    + "AND reportData.date = :date "
                    + "ORDER BY reportData.date ASC")
                .setParameter("user", user)
                .setParameter("date", date)
                .getResultList();
    }

    @Nonnull
    public List<ReportData> findByUserAndDateBetweenOrderByDateAsc(
            @Nonnull final User user,
            @Nonnull final Date startDate,
            @Nonnull final Date endDate
    ) {
        return entityManager.createQuery(
                "SELECT reportData FROM ReportData reportData "
                        + "WHERE reportData.user = :user "
                        + "AND reportData.date >= :startDate "
                        + "AND reportData.date <= :endDate "
                        + "ORDER BY reportData.date ASC")
                .setParameter("user", user)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }

}
