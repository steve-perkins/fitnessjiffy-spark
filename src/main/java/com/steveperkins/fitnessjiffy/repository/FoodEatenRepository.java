package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.Food;
import com.steveperkins.fitnessjiffy.domain.FoodEaten;
import com.steveperkins.fitnessjiffy.domain.User;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class FoodEatenRepository implements Repository<FoodEaten, UUID> {

    private final EntityManager entityManager;

    public FoodEatenRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public FoodEaten findOne(UUID key) {
        return entityManager.find(FoodEaten.class, key);
    }

    @Override
    public List<FoodEaten> findAll() {
        return entityManager.createQuery("SELECT foodEaten FROM FoodEaten foodEaten").getResultList();
    }

    @Override
    public void save(FoodEaten entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(FoodEaten entity) {
        entityManager.remove(entity);
    }

    @Nonnull
    public List<FoodEaten> findByUserEqualsOrderByDateAsc(@Nonnull final User user) {
        return entityManager.createQuery(
                "SELECT foodEaten FROM FoodEaten foodEaten WHERE foodEaten.user = :user ORDER BY foodEaten.date asc")
                .setParameter("user", user)
                .getResultList();
    }

    @Nonnull
    public List<FoodEaten> findByUserEqualsAndFoodEqualsOrderByDateAsc(
            @Nonnull final User user,
            @Nonnull final Food food
    ) {
        return entityManager.createQuery(
                "SELECT foodEaten FROM FoodEaten foodEaten "
                + "WHERE foodEaten.user = :user "
                + "AND foodEaten.food = :food "
                + "ORDER BY foodEaten.date asc")
                .setParameter("user", user)
                .setParameter("food", food)
                .getResultList();
    }

    @Nonnull
    public List<FoodEaten> findByUserEqualsAndDateEquals(
            @Nonnull final User user,
            @Nonnull final Date date
    ) {
        return entityManager.createQuery(
                "SELECT foodEaten FROM FoodEaten foodEaten, Food food "
                        + "WHERE foodEaten.food = food "
                        + "AND foodEaten.user = :user "
                        + "AND foodEaten.date = :date "
                        + "ORDER BY food.name ASC")
                .setParameter("user", user)
                .setParameter("date", date)
                .getResultList();
    }

    @Nonnull
    public List<Food> findByUserEatenWithinRange(
            @Nonnull final User user,
            @Nonnull final Date startDate,
            @Nonnull final Date endDate
    ) {
        return entityManager.createQuery(
                "SELECT DISTINCT food FROM Food food, FoodEaten foodEaten "
                        + "WHERE food = foodEaten.food "
                        + "AND foodEaten.user = :user "
                        + "AND foodEaten.date BETWEEN :startDate AND :endDate "
                        + "ORDER BY food.name ASC")
                .setParameter("user", user)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }

}
