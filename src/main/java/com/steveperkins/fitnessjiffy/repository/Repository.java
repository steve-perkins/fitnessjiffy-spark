package com.steveperkins.fitnessjiffy.repository;

import java.util.List;

public interface Repository<T, K> {

    T findOne(K key);

    List<T> findAll();

    void save(T entity);

    void delete(T entity);

    default int count() {
        return findAll().size();
    }

}
