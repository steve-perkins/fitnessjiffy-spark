package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.Exercise;
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed;
import com.steveperkins.fitnessjiffy.domain.User;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class ExercisePerformedRepository implements Repository<ExercisePerformed, UUID> {

    private final EntityManager entityManager;

    public ExercisePerformedRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public ExercisePerformed findOne(UUID key) {
        return entityManager.find(ExercisePerformed.class, key);
    }

    @Override
    public List<ExercisePerformed> findAll() {
        return entityManager.createQuery("SELECT exercisePerformed FROM ExercisePerformed exercisePerformed")
                .getResultList();
    }

    @Override
    public void save(ExercisePerformed entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(ExercisePerformed entity) {
        entityManager.remove(entity);
    }

    @Nonnull
    public List<ExercisePerformed> findByUserEqualsAndDateEquals(
            @Nonnull final User user,
            @Nonnull final Date date
    ) {
        return entityManager.createQuery(
                "SELECT exercisePerformed FROM ExercisePerformed exercisePerformed, Exercise exercise "
                        + "WHERE exercisePerformed.exercise = exercise "
                        + "AND exercisePerformed.user = :user "
                        + "AND exercisePerformed.date = :date "
                        + "ORDER BY exercise.description ASC")
                .setParameter("user", user)
                .setParameter("date", date)
                .getResultList();
    }

    @Nonnull
    public List<Exercise> findByUserPerformedWithinRange(
            @Nonnull final User user,
            @Nonnull final Date startDate,
            @Nonnull final Date endDate
    ) {
        return entityManager.createQuery(
                "SELECT DISTINCT exercise FROM Exercise exercise, ExercisePerformed exercisePerformed "
                        + "WHERE exercise = exercisePerformed.exercise "
                        + "AND exercisePerformed.user = :user "
                        + "AND exercisePerformed.date BETWEEN :startDate AND :endDate "
                        + "ORDER BY exercise.description ASC")
                .setParameter("user", user)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }

}
