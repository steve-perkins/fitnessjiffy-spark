package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

public class UserRepository implements Repository<User, UUID> {

    private final EntityManager entityManager;

    public UserRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User findOne(UUID key) {
        return entityManager.find(User.class, key);
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT user FROM User user").getResultList();
    }

    @Override
    public void save(User entity) {
        if (!entityManager.isJoinedToTransaction()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @Override
    public void delete(User entity) {
        entityManager.remove(entity);
    }

    @Nullable
    public User findByEmailEquals(@Nonnull final String email) {
        return (User) entityManager.createQuery(
                "SELECT user FROM User user WHERE user.email = :email")
                .setParameter("email", email)
                .getSingleResult();
    }

}
