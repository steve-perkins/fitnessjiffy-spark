package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.User;
import com.steveperkins.fitnessjiffy.domain.Weight;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class WeightRepository implements Repository<Weight, UUID> {

    private final EntityManager entityManager;

    public WeightRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Weight findOne(UUID key) {
        return entityManager.find(Weight.class, key);
    }

    @Override
    public List<Weight> findAll() {
        return entityManager.createQuery("SELECT weight FROM Weight weight").getResultList();
    }

    @Override
    public void save(Weight entity) {
        if (!entityManager.isJoinedToTransaction()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @Override
    public void delete(Weight entity) {
        if (!entityManager.isJoinedToTransaction()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(entity);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @Nonnull
    public List<Weight> findByUserOrderByDateDesc(@Nonnull final User user) {
        return entityManager.createQuery(
                "SELECT weight FROM Weight weight WHERE weight.user = :user")
                .setParameter("user", user)
                .getResultList();
    }

    /**
     * Unfortunately, this method is using a native query because JPQL does not support the "LIMIT" keyword.
     * Alternatives would include using a JPQL query built with a subselect, or using Spring Data JPA pagination...
     * but a native query is perhaps the least ugly of all evils.  Also, this is meant to be a demo and teaching
     * application anyway, so why not show a native query example somewhere in the mix?
     */
    @Nullable
    public Weight findByUserMostRecentOnDate(
            @Nonnull final User user,
            @Nonnull final Date date
    ) {
        return (Weight) entityManager.createNativeQuery(
                "SELECT weight.* FROM weight, fitnessjiffy_user "
                        + "WHERE weight.user_id = fitnessjiffy_user.id "
                        + "AND fitnessjiffy_user.id = ?1 "
                        + "AND weight.date <= ?2 "
                        + "ORDER BY weight.date DESC LIMIT 1", Weight.class)
                .setParameter(1, user.getId())
                .setParameter(2, date)
                .getSingleResult();
    }

    /**
     * "findByUserMostRecentOnDate" is used for purposes of display, and for report generation, to account for days
     * on which weight entry might have been skipped.  "findByUserAndDate", however, looks only on the specified
     * date with no adjustment... for purposes of updating a particular weight entry correctly.
     */
    @Nullable
    public Weight findByUserAndDate(
            @Nonnull final User user,
            @Nonnull final Date date
    ) {
        return (Weight) entityManager.createQuery(
                "SELECT weight FROM Weight weight WHERE weight.user = :user AND weight.date = :date")
                .setParameter("user", user)
                .setParameter("date", date)
                .getSingleResult();
    }

}
