package com.steveperkins.fitnessjiffy.repository;

import com.steveperkins.fitnessjiffy.domain.Exercise;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

public class ExerciseRepository implements Repository<Exercise, UUID> {

    private final EntityManager entityManager;

    public ExerciseRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Exercise findOne(UUID key) {
        return entityManager.find(Exercise.class, key);
    }

    @Override
    public List<Exercise> findAll() {
        return entityManager.createQuery("SELECT exercise from Exercise exercise").getResultList();
    }

    @Override
    public void save(Exercise entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(Exercise entity) {
        entityManager.remove(entity);
    }

    @Nonnull
    public List<String> findAllCategories() {
        return entityManager.createQuery(
            "SELECT DISTINCT(exercise.category) FROM Exercise exercise ORDER BY exercise.category"
        ).getResultList();
    }

    @Nonnull
    public List<Exercise> findByCategoryOrderByDescriptionAsc(@Nonnull final String category) {
        return entityManager.createQuery(
                "SELECT exercise FROM Exercise exercise WHERE category = :category ORDER BY exercise.description DESC")
                .setParameter("category", category)
                .getResultList();
    }

    @Nonnull
    public List<Exercise> findByDescriptionLike(@Nonnull final String description) {
        return entityManager.createQuery(
                "SELECT exercise FROM Exercise exercise "
                        + "WHERE LOWER(exercise.description) LIKE LOWER(CONCAT('%', :description, '%')) "
                        + "ORDER BY exercise.description ASC")
                .setParameter("description", description)
                .getResultList();
    }

}
