package com.steveperkins.fitnessjiffy;

import com.steveperkins.fitnessjiffy.controller.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.steveperkins.fitnessjiffy.controller.ProfileController;
import spark.template.thymeleaf.ThymeleafTemplateEngine;

import static spark.Spark.*;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);
    private static EntityManagerFactory entityManagerFactory;
    private static Properties properties = new Properties();
    static{
        try (final InputStream in = Application.class.getResourceAsStream("/application.properties")) {
            properties.load(in);
            in.close();
        } catch (IOException e) {
            logger.error("An error occurred loading the properties file", e);
        }
    }

    public static void main(String[] args) {
        initDatabase();
        initPort();
        initRoutes();
    }

    public static Properties getProperties() {
        return properties;
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private static void initDatabase() {
        entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
        if ("true".equalsIgnoreCase(properties.getProperty("flyway.enabled"))) {
            // TODO: Flyway
        }
    }

    private static void initPort() {
        int portNumber = 8080;
        try {
            portNumber = Integer.parseInt(properties.getProperty("server.port"));
        } catch (Exception e) {
        }
        port(portNumber);
    }

    private static void initRoutes() {
        staticFiles.location("/public");

        get("/login", new LoginController().viewLoginPage(), new ThymeleafTemplateEngine());
        post("/login", new LoginController().doLogin(), new ThymeleafTemplateEngine());
        get("/login/error", new LoginController().viewLoginError(), new ThymeleafTemplateEngine());
        post("/logout", new LoginController().doLogout(), new ThymeleafTemplateEngine());

        get("/", new ProfileController().viewMainProfilePage(), new ThymeleafTemplateEngine());
        get("/profile", new ProfileController().viewMainProfilePage(), new ThymeleafTemplateEngine());
        post("/profile/save", new ProfileController().updateProfile(), new ThymeleafTemplateEngine());
        post("/profile/weight/save", new ProfileController().createOrUpdateWeight(), new ThymeleafTemplateEngine());
    }

}