package com.steveperkins.fitnessjiffy.test;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public abstract class AbstractTest {

    static {
        new File("h2test.mv.db").delete();
        new File("h2test.trace.db").delete();
    }

    final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void before() throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final Session session = entityManager.unwrap(Session.class);
        session.doWork((connection) -> {
            final Statement statement = connection.createStatement();
            statement.execute("DROP ALL OBJECTS");
            statement.execute("RUNSCRIPT FROM 'classpath:/backup.sql'");
        });
        entityManager.getTransaction().commit();
    }

    @After
    public void after() throws InterruptedException {
//        while (!reportDataService.isIdle()) {
//            Thread.sleep(TimeUnit.SECONDS.toMillis(1));
//        }
    }

}
